<?php
/**
 * ebay.php
 *
 * @package eBay Turbo Lister 2.0 Exporter
 * @copyright Copyright 2007 Numinix Technology http://www.numinix.com
 * @copyright Portions Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: ebay_tools_dhtml.php, v1.0.0 30.07.2007 22:18 numinix $
 */
$za_contents[] = array('text' => BOX_EBAY, 'link' => zen_href_link(FILENAME_EBAY, '', 'NONSSL'));
?>