<?php
/**
 * ebay.php
 *
 * @package eBay Turbo Lister 2.0 Exporter
 * @copyright Copyright 2007 Numinix Technology http://www.numinix.com
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: epier.php, v1.0.0 30.07.2007 22:17 numinix $
 */

define('FILENAME_EBAY', 'ebay');
define('BOX_CONFIGURATION_EBAYEXPORTER', 'eBay Exporter Configuration');
?>