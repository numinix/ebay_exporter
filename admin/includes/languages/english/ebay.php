<?php
/**
 * ebay.php
 *
 * @package eBay Exporter
 * @copyright Copyright 2007 Numinix Technology http://www.numinix.com
 * @copyright Portions Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: ebay.php, v1.0.1 16.08.2007 12:11 numinix $
 */

define('HEADING_TITLE', 'eBay Exporter');
define('TEXT_EBAY_OVERVIEW_HEAD', '<p><strong>OVERVIEW:</strong></p>');
define('TEXT_EBAY_OVERVIEW_TEXT', '<p>This module automatically generates an eBay bulk file from your Zen Cart store.</p>');
define('TEXT_EBAY_INSTRUCTIONS_HEAD', '<p><strong>INSTRUCTIONS: </strong></p>');
define('TEXT_EBAY_INSTRUCTIONS_STEP1', '<p><strong><font color="#FF0000">STEP 1:</font></strong> Click <a href=%s><strong>[HERE]</strong></a> to create / update your product feed. </p>');
define('TEXT_EBAY_INSTRUCTIONS_STEP1_NOTE', '<p>NOTE: You may <a href="' . HTTP_SERVER . DIR_WS_CATALOG . EBAY_DIRECTORY . EBAY_OUTPUT_FILENAME . '" target="_blank" class="splitPageLink"><strong>view</strong></a> your product feed before uploading to eBay.com. </p>');

?>