<?php
/**
 * ebay.php
 *
 * @package eBay Exporter
 * @copyright Copyright 2006-2008 Numinix Technology http://www.numinix.com
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: ebay.php, v1.02a 04.23.2008 22:37:59 numinix $
 *
 */
	@define('EBAY_VERSION', '1.02a 04.23.2008 22:37:59');

	require('includes/application_top.php');
	
	@define('EBAY_OUTPUT_BUFFER_MAXSIZE', 1024*1024);
	@define('EBAY_CHECK_IMAGE', 'false');
	@define('EBAY_STAT', false);
	$anti_timeout_counter = 0; //for timeout issues as well as counting number of products processed
	$max_limit = false;
	$today = date("Y-m-d");
	$ebay_start_counter = 0;
	@define('EBAY_USE_CPATH', 'false');
	@define('NL', "<br />\n");

	require(zen_get_file_directory(DIR_WS_LANGUAGES . $_SESSION['language'] .'/', 'ebay.php', 'false'));

	$languages = $db->execute("select code, languages_id from " . TABLE_LANGUAGES . " where name='" . 'English' . "' limit 1");

	$product_url_add = ('true' == 'true' ? "&language=" . $languages->fields['code'] : '') . ('true' == 'true' ? "&currency=" . zen_get_currency(EBAY_CURRENCY) : '');

	echo TEXT_EBAY_STARTED . NL;
	echo TEXT_EBAY_FILE_LOCATION . DIR_FS_CATALOG . EBAY_DIRECTORY . EBAY_OUTPUT_FILENAME . NL;
	echo "Processing: Feed - " . (isset($_GET['feed']) && $_GET['feed'] == "yes" ? "Yes" : "No") . ", Upload - " . (isset($_GET['upload']) && $_GET['upload'] == "yes" ? "Yes" : "No") . NL;

if (isset($_GET['feed']) && $_GET['feed'] == "yes") {
	if (is_dir(DIR_FS_CATALOG . EBAY_DIRECTORY)) {
		if (!is_writeable(DIR_FS_CATALOG . EBAY_DIRECTORY)) {
			echo ERROR_EBAY_DIRECTORY_NOT_WRITEABLE . NL;
			die;
		}
	} else {
		echo ERROR_EBAY_DIRECTORY_DOES_NOT_EXIST . NL;
		die;
	}

	$stimer_feed = microtime_float();
	if (!get_cfg_var('safe_mode') && function_exists('safe_mode')) {
		set_time_limit(0);
	}

	$output_buffer = "";

	if(!zen_ebay_fwrite()) {
		echo ERROR_EBAY_OPEN_FILE . NL;
		die;
	}

	$output = array();
	$output["Site"] = "Site";
	$output["Format"] = "Format";
	$output["Currency"] = "Currency";
	$output["Title"] = "Title";
	$output["SubtitleText"] = "SubtitleText";
	$output["Custom Label"] = "Custom Label";
	$output["Description"] = "Description";
	$output["Category 1"] = "Category 1";
	$output["Category 2"] = "Category 2";
	$output["Store Category"] = "Store Category";
	$output["Store Category 2"] = "Store Category 2";
	$output["PicURL"] = "PicURL";
	$output["Quantity"] = "Quantity";
	$output["LotSize"] = "LotSize";
	$output["Duration"] = "Duration";
	$output["Start Price"] = "Starting Price";
	$output["Reserve Price"] = "Reserve Price";
	$output["BIN Price"] = "BIN Price";
	$output["Private Auction"] = "Private Auction";
	$output["Counter"] = "Counter";
	$output["Buyer pays shipping"] = "Buyer pays shipping";
	$output["Payment Instructions"] = "Payment Instructions";
	$output["Specifying Shipping Costs"] = "Specifying Shipping Costs";
	$output["Insurance Option"] = "Insurance Option";
	$output["Insurance Amount"] = "Insurance Amount";
	$output["Sales Tax Amount"] = "Sales Tax Amount";
	$output["Sales Tax State"] = "Sales Tax State";
	$output["Apply tax to total"] = "Apply tax to total";
	$output["Accept PayPal"] = "Accept PayPal";
	$output["PayPal Email Address"] = "PayPal Email Address";
	$output["Accept MO Cashiers"] = "Accept MO Cashiers";
	$output["Accept Personal Check"] = "Accept Personal Check";
	$output["Accept Visa/Mastercard"] = "Accept Visa/Mastercard";
	$output["Accept AmEx"] = "Accept AmEx";
	$output["Accept Discover"] = "Accept Discover";
	$output["Accept Payment Other"] = "Accept Payment Other";
	$output["Accept Payment Other Online"] = "Accept Payment Other Online";
	$output["Accept COD"] = "Accept COD";
	$output["COD PrePay Delivery"] = "COD PrePay Delivery";
	$output["Postal Transfer"] = "Postal Transfer";
	$output["Payment See Description"] = "Payment See Description";
	$output["Accept Money Xfer"] = "Accept Money Xfer";
	$output["CCAccepted"] = "CCAccepted";
	$output["CashOnPickupAccepted"] = "CashOnPickupAccepted";
	$output["MoneyXferAccepted"] = "MoneyXferAcceptedinCheckout";
	$output["MoneyXferAcceptedinCheckout"] = "MoneyXferAcceptedinCheckout";
	$output["Ship-To Option"] = "Ship-To Option";
	$output["Escrow"] = "Escrow";
	$output["BuyerPaysFixed"] = "BuyerPaysFixed";
	$output["Location - City/State"] = "Location - City/State";
	$output["Location - Region"] = "Location - Region";
	$output["Location - Country"] = "Location - Country";
	$output["Title Bar Image"] = "Title Bar Image";
	$output["Gallery1.Gallery"] = "Gallery1.Gallery";
	$output["Gallery Featured"] = "Gallery Featured";
	$output["Gallery URL"] = "Gallery URL";
	$output["PicInDesc"] = "PicInDesc";
	$output["PhotoOneRadio"] = "PhotoOneRadio";
	$output["PhotoOneURL"] = "PhotoOneURL";
	$output["Gallery2.GalleryPlus"] = "Gallery2.GalleryPlus";	
	$output["Bold"] = "Bold";
	$output["MotorsGermanySearchable"] = "MotorsGermanySearchable";
	$output["Border"] = "Border";
	$output["LE.Highlight"] = "LE.Highlight";
	$output["Featured Plus"] = "Featured Plus";
	$output["Home Page Featured"] = "Home Page Featured";
	$output["Subtitle in search results"] = "Subtitle in search results";	
	$output["GiftIcon"] = "GiftIcon";
	$output["DepositType"] = "DepositType";
	$output["DepositAmount"] = "DepositAmount";
	$output["ShippingRate"] = "ShippingRate";
	$output["ShippingCarrier"] = "ShippingCarrier";
	$output["ShippingType"] = "ShippingType";
	$output["ShippingPackage"] = "ShippingPackage";
	$output["ShippingIrregular"] = "ShippingIrregular";
	$output["ShippingWeightUnit"] = "ShippingWeightUnit";
	$output["WeightMajor"] = "WeightMajor";
	$output["WeightMinor"] = "WeightMinor";
	$output["PackageDimension"] = "PackageDimension";
	$output["ShipFromZipCode"] = "ShipFromZipCode";
	$output["PackagingHandlingCosts"] = "PackagingHandlingCosts";
	$output["Year"] = "Year";
	$output["MakeCode"] = "MakeCode";
	$output["ModelCode"] = "ModelCode";
	$output["EngineCode"] = "EngineCode";
	$output["ThemeId"] = "ThemeId";
	$output["LayoutId"] = "LayoutId";
	$output["AutoPay"] = "AutoPay";
	$output["Apply Multi-item Shipping Discount"] = "Apply Multi-item Shipping Discount";
	$output["Attributes"] = "Attributes";
	$output["Package Length"] = "Package Length";
	$output["Package Width"] = "Package Width";
	$output["Package Depth"] = "Package Depth";
	$output["ShippingServiceOptions"] = "ShippingServiceOptions";
	$output["VATPercent"] = "VATPercent";
	$output["ProductID"] = "ProductID";
	$output["UseStockPhotoURLAsGallery"] = "UseStockPhotoURLAsGallery";
	$output["IncludeStockPhotoURL"] = "IncludeStockPhotoURL";
	$output["IncludeProductInfo"] = "IncludeProductInfo";
	$output["UniqueIdentifier"] = "UniqueIdentifier";
	$output["GiftIcon.GiftWrap"] = "GiftIcon.GiftWrap";
	$output["GiftIcon.GiftExpressShipping"] = "GiftIcon.GiftExpressShipping";
	$output["GiftIcon.GiftShipToRecipient"] = "GiftIcon.GiftShipToRecipient";
	$output["InternationalShippingServiceOptions"] = "InternationalShippingServiceOptions";
	$output["Ship-To Locations"] = "Ship-To Locations";
	$output["Zip"] = "Zip";
	$output["NowAndNew"] = "NowAndNew";
	$output["BuyerRequirements/LinkedPayPalAccount"] = "BuyerRequirements/LinkedPayPalAccount";
	$output["PM.PaisaPayAccepted"] = "PM.PaisaPayAccepted";
	$output["LE.ProPackBundle"] = "LE.ProPackBundle";
	$output["BestOfferEnabled"] = "BestOfferEnabled";
	$output["LiveAuctionDetails/LotNumber"] = "LiveAuctionDetails/LotNumber";
	$output["LiveAuctionDetails/SellerSalesNumber"] = "LiveAuctionDetails/SellerSalesNumber";
	$output["LiveAuctionDetails/LowEstimate"] = "LiveAuctionDetails/LowEstimate";
	$output["LiveAuctionDetails/HighEstimate"] = "LiveAuctionDetails/HighEstimate";
	$output["LiveAuctionDetails/eBayBatchNumber"] = "LiveAuctionDetails/eBayBatchNumber";
	$output["LiveAuctionDetails/eBayItemInBatch"] = "LiveAuctionDetails/eBayItemInBatch";
	$output["LiveAuctionDetails/ScheduleID"] = "LiveAuctionDetails/ScheduleID";
	$output["LiveAuctionDetails/UserCatalogID"] = "LiveAuctionDetails/UserCatalogID";
	$output["Item.ExportedImages"] = "Item.ExportedImages";
	$output["PhotoDisplayType"] = "PhotoDisplayType";
	$output["TaxTable"] = "TaxTable";
	$output["LoanCheck"] = "LoanCheck";
	$output["CashInPerson"] = "CashInPerson";
	$output["HoursToDeposit"] = "HoursToDeposit";
	$output["DaysToFullPayment"] = "DaysToFullPayment";
	$output["UserHostedOptimizePictureWellBitmap"] = "UserHostedOptimizePictureWellBitmap";
	$output["BuyerResponsibleForShipping"] = "BuyerResponsibleForShipping";
	$output["GetItFast"] = "GetItFast";
	$output["DispatchTimeMax"] = "DispatchTimeMax";
	$output["DigitalDeliveryDetails/Requirements"] = "DigitalDeliveryDetails/Requirements";
	$output["DigitalDeliveryDetails/Method"] = "DigitalDeliveryDetails/Method";
	$output["DigitalDeliveryDetails/Instructions"] = "DigitalDeliveryDetails/Instructions";
	$output["DigitalDeliveryDetails/URL"] = "DigitalDeliveryDetails/URL";
	$output["CharityID"] = "CharityID";
	$output["CharityName"] = "CharityName";
	$output["DonationPercentage"] = "DonationPercentage";
	$output["AutoDecline"] = "AutoDecline";
	$output["ListingDetails/MinimumBestOfferPrice"] = "ListingDetails/MinimumBestOfferPrice";
	$output["ListingDetails/MinimumBestOfferMessage"] = "ListingDetails/MinimumBestOfferMessage";
	$output["LE.ValuePackBundle"] = "LE.ValuePackBundle";
	$output["LE.ProPackPlusBundle"] = "LE.ProPackPlusBundle";
	$output["LE.BasicUpgradePackBundle"] = "LE.BasicUpgradePackBundle";
	$output["LocalOnlyChk"] = "LocalOnlyChk";
	$output["ListingDetails/LocalListingDistance"] = "ListingDetails/LocalListingDistance";
	$output["SkypeChat"] = "SkypeChat";
	$output["SkypeVoice"] = "SkypeVoice";
	$output["SkypeName"] = "SkypeName";
	$output["ContactPrimaryPhone"] = "ContactPrimaryPhone";																												
	$output["ContactSecondaryPhone"] = "ContactSecondaryPhone";
	$output["ExtendedSellerContactDetails/ClassifiedAdContactByEmailEnabled"] = "ExtendedSellerContactDetails/ClassifiedAdContactByEmailEnabled";
	$output["ppl_PhoneEnabled"] = "ppl_PhoneEnabled";
	$output["BuyerRequirements/ShipToRegistrationCountry"] = "BuyerRequirements/ShipToRegistrationCountry";
	$output["BuyerRequirements/ZeroFeedbackScore"] = "BuyerRequirements/ZeroFeedbackScore";
	$output["BuyerRequirements/MinimumFeedbackScore"] = "BuyerRequirements/MinimumFeedbackScore";
	$output["BuyerRequirements/MaximumUnpaidItemStrikes"] = "BuyerRequirements/MaximumUnpaidItemStrikes";
	$output["BuyerRequirements/MaximumItemRequirements/MaximumItemCount"] = "BuyerRequirements/MaximumItemRequirements/MaximumItemCount";
	$output["BuyerRequirements/MaximumItemRequirements/MinimumFeedbackScore"] = "BuyerRequirements/MaximumItemRequirements/MinimumFeedbackScore";
	$output["BuyerRequirements/VerifiedUserRequirements/VerifiedUser"] = "BuyerRequirements/VerifiedUserRequirements/VerifiedUser";
	$output["BuyerRequirements/VerifiedUserRequirements/MinimumFeedbackScore"] = "BuyerRequirements/VerifiedUserRequirements/MinimumFeedbackScore";
	$output["DisableBuyerRequirements"] = "DisableBuyerRequirements";
	$output["Domestic Insurance Option"] = "Domestic Insurance Option";
	$output["Domestic Insurance Amount"] = "Domestic Insurance Amount";
	$output["InternationalShippingType"] = "InternationalShippingType";
	$output["InternationalPackagingHandlingCosts"] = "InternationalPackagingHandlingCosts";
	$output["ProStores Name"] = "ProStores Name";
	$output["ProStores Enabled"] = "ProStores Enabled";
	$output["Domestic Profile Discount"] = "Domestic Profile Discount";
	$output["International Profile Discount"] = "International Profile Discount";
	$output["Apply Profile Domestic"] = "Apply Profile Domestic";
	$output["Apply Profile International"] = "Apply Profile International";



	
	
	zen_ebay_fwrite($output);
	

	$categories_array = zen_ebay_category_tree();
	

	
	
	if (EBAY_ASA == 'false') {
			$products_query = "SELECT distinct(p.products_id), p.products_model, p.products_weight, pd.products_name, pd.products_description, p.products_image, p.products_tax_class_id, p.products_price_sorter, GREATEST(p.products_date_added, IFNULL(p.products_last_modified, 0), IFNULL(p.products_date_available, 0)) AS base_date, m.manufacturers_name, cd.categories_id, p.products_quantity, pt.type_handler, s.specials_new_products_price, s.expires_date
							 FROM " . TABLE_PRODUCTS . " p
							   LEFT JOIN " . TABLE_MANUFACTURERS . " m ON (p.manufacturers_id = m.manufacturers_id)
																 
								 LEFT JOIN " . TABLE_PRODUCTS_TO_CATEGORIES . " pc ON (p.products_id = pc.products_id)
                 LEFT JOIN " . TABLE_CATEGORIES_DESCRIPTION . " cd ON (pc.categories_id = cd.categories_id)
                  LEFT JOIN " . TABLE_CATEGORIES . " c ON (cd.categories_id = c.categories_id)
            
								 LEFT JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd ON (p.products_id = pd.products_id)
								 LEFT JOIN " . TABLE_PRODUCT_TYPES . " pt ON (p.products_type=pt.type_id)
								 LEFT JOIN " . TABLE_SPECIALS . " s ON (s.products_id = p.products_id)
  							 WHERE p.products_status = 1
								 AND p.product_is_call = 0
								 AND p.product_is_free = 0
								 AND pd.language_id = " . (int)$languages->fields['languages_id'] ."
               GROUP BY p.products_id
							 ORDER BY p.products_id ASC";
	} elseif (EBAY_ASA == 'true') {
			$products_query = "SELECT distinct(p.products_id), p.products_model, p.products_weight, pd.products_name, pd.products_description, p.products_image, p.products_tax_class_id, p.products_price_sorter, GREATEST(p.products_date_added, IFNULL(p.products_last_modified, 0), IFNULL(p.products_date_available, 0)) AS base_date, m.manufacturers_name, cd.categories_id, p.products_quantity, pt.type_handler, s.specials_new_products_price, s.expires_date, p.products_ebay, p.products_length, p.products_width, p.products_height, p.products_upc, p.products_weight_type, p.products_dim_type, p.products_phrases
								 FROM " . TABLE_PRODUCTS . " p
									 LEFT JOIN " . TABLE_MANUFACTURERS . " m ON (p.manufacturers_id = m.manufacturers_id)
									 LEFT JOIN " . TABLE_PRODUCTS_TO_CATEGORIES . " pc ON (p.products_id = pc.products_id)
                 LEFT JOIN " . TABLE_CATEGORIES_DESCRIPTION . " cd ON (pc.categories_id = cd.categories_id)
                  LEFT JOIN " . TABLE_CATEGORIES . " c ON (cd.categories_id = c.categories_id)
									 LEFT JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd ON (p.products_id = pd.products_id)
									 LEFT JOIN " . TABLE_PRODUCT_TYPES . " pt ON (p.products_type=pt.type_id)
									 LEFT JOIN " . TABLE_SPECIALS . " s ON (s.products_id = p.products_id)
								 WHERE p.products_status = 1
									 AND p.product_is_call = 0
									 AND p.product_is_free = 0
									 AND pd.language_id = " . (int)$languages->fields['languages_id'] ."
                 GROUP BY p.products_id
								 ORDER BY p.products_id ASC";
	}
	$products = $db->Execute($products_query);
	$tax_rate = array();
		while (!$products->EOF && !$max_limit) { // run until end of file or until maximum number of products reached
			list($categories_list, $cPath) = zen_ebay_get_category($products->fields['products_id']);
			$ebay_start_counter++;
			if (numinix_categories_check(EBAY_POS_CATEGORIES, $products->fields['products_id'], 1) == true && numinix_categories_check(EBAY_NEG_CATEGORIES, $products->fields['products_id'], 2) == false && zen_ebay_keywords(EBAY_NEG_KEYWORDS, $products->fields['products_name'], 2) == false && ($ebay_start_counter >= EBAY_START_PRODUCTS)) { // check to see if category limits are set.  If so, only process for those categories.
				if ($anti_timeout_counter == EBAY_MAX_PRODUCTS && EBAY_MAX_PRODUCTS != 0) { // if counter is greater than or equal to maximum products
					$max_limit = true; // then max products reached
				} else {
					$max_limit = false; // otherwise, max products not reached
				} 
				$price = zen_get_products_actual_price($products->fields['products_id']);
				if ($price > 0 && zen_price_check($price) == true) {
					$anti_timeout_counter++;
					if (!isset($tax_rate[(int)$products->fields['products_tax_class_id']])) {
						$tax_rate[(int)$products->fields['products_tax_class_id']] = zen_get_tax_rate((int)$products->fields['products_tax_class_id']);
						$price = zen_add_tax($price, $tax_rate[(int)$products->fields['products_tax_class_id']]);
					}
					$price = $currencies->value($price, true, zen_get_currency(EBAY_CURRENCY), $currencies->get_value(zen_get_currency(EBAY_CURRENCY)));
					$href = ($products->fields['type_handler'] ? $products->fields['type_handler'] : 'product') . '_info';
					$cPath_href = (EBAY_USE_CPATH == 'true' ? 'cPath=' . $cPath . '&' : '');
					
					$output = array();					
					$output["Site"] = EBAY_SITEID;
					$output["Format"] = EBAY_FORMAT;
					$output["Currency"] = EBAY_CURRENCY;
					$output["Title"] = '"' . zen_ebay_cleaner(ShortenText($products->fields['products_name'], 55)) . '"';
					
					if (EBAY_ASA == 'true') {
						$phrases = explode(",", $products->fields['products_phrases']);
						$subtitle = ShortenText($phrases["0"], 55);
					}
					$output["SubtitleText"] = (EBAY_SUBTITLE == 'true' && EBAY_ASA == 'true' ? $subtitle : "");
					
					if (EBAY_LABEL == 'upc' && EBAY_ASA == 'true') {
						$output["Custom Label"] = $products->fields['products_upc'];
					} else if (EBAY_LABEL == 'model') {
						$output["Custom Label"] = $products->fields['products_model'];
					} else {
						$output["Custom Label"] = $products->fields['products_id'];
					}
					
					$output["Description"] = '"' . zen_ebay_cleaner(EBAY_TEMPLATE_TOP) . zen_ebay_sanita($products->fields['products_description']) . zen_ebay_cleaner(EBAY_TEMPLATE_BOTTOM) . '"';

					if (EBAY_ASA == 'true') {
					$output["Category 1"] = $products->fields['products_ebay'];
					} else {
				$output["Category 1"] = EBAY_MAIN_CATEGORIES;
	
					}
					$output["Category 2"] = "";
					if (EBAY_ASA == 'true') {
						$output["Store Category"] = product_category($products->fields['products_ebay'], 2);
					} else {
						$output["Store Category"] = "0";
					}
					$output["Store Category 2"] = "0";
					$image = trim(zen_ebay_image_url($products->fields['products_image']));
					$output["PicURL"] = $image;
					
 					if (EBAY_QUANTITY == '') {
						$output["Quantity"] = $products->fields['products_quantity'];
					} else {
						$output["Quantity"] = EBAY_QUANTITY;
					}
					
					$output["LotSize"] = "~";
					$output["Duration"] = EBAY_DURATION;
					if (EBAY_FIXED_STARTING == '') {
						$output["Start Price"] = number_format(EBAY_STARTING / 100 * $price, 2, '.', '');
					} else {
						$output["Start Price"] = EBAY_FIXED_STARTING;
					}
					if (EBAY_RESERVE == 0) {
						$output["Reserve Price"] = "~";
					} else {
						$output["Reserve Price"] = number_format(EBAY_RESERVE / 100 * $price, 2, '.', '');
					}
					if (EBAY_BIN == 0) {
						$output["BIN Price"] = "~";
					} else {
						$output["BIN Price"] = number_format(EBAY_BIN / 100 * $price, 2, '.', '');
					}
					$output["Private Auction"] = EBAY_PRIVATE;
					$output["Counter"] = EBAY_COUNTER;
					if (EBAY_PAYS_SHIPPING == 'buyer') { 
						$output["Buyer pays shipping"] = 1;
					} else if (EBAY_PAYS_SHIPPING == 'seller') {
						$output["Buyer pays shipping"] = 0;
					}
					$output["Payment Instructions"] = "~";
					$output["Specifying Shipping Costs"] = "~";
					$output["Insurance Option"] = EBAY_INSURANCE;
					$output["Insurance Amount"] = "~";
					$output["Sales Tax Amount"] = "~";
					$output["Sales Tax State"] = "~";
					$output["Apply tax to total"] = EBAY_TAX;
					$output["Accept PayPal"] = EBAY_PAYPAL_ACCEPTED;
					$output["PayPal Email Address"] = '"' . EBAY_PAYPAL_ADDRESS . '"';
					$output["Accept MO Cashiers"] = EBAY_MONEYORDER;
					$output["Accept Personal Check"] = EBAY_CHECK;
					$output["Accept Visa/Mastercard"] = EBAY_VISA;
					$output["Accept AmEx"] = EBAY_AMEX;
					$output["Accept Discover"] = EBAY_DISCOVER;
					$output["Accept Payment Other"] = EBAY_OTHER_PAYMENT;
					$output["Accept Payment Other Online"] = EBAY_OTHER_ONLINE;
					$output["Accept COD"] = EBAY_COD;
					$output["COD PrePay Delivery"] = EBAY_COD_PREPAY;
					$output["Postal Transfer"] = EBAY_POSTAL_TRANSFER;
					$output["Payment See Description"] = EBAY_SEE_DESCRIPTION;
					$output["Accept Money Xfer"] = EBAY_MX;
					$output["CCAccepted"] = EBAY_CC;
					$output["CashOnPickupAccepted"] = EBAY_PICKUP;
					$output["MoneyXferAccepted"] = EBAY_MX;
					$output["MoneyXferAcceptedinCheckout"] = EBAY_MX_CHECKOUT;
					$output["Ship-To Option"] = "~";
					$output["Escrow"] = 0;
					$output["BuyerPaysFixed"] = 0;
					$output["Location - City/State"] = EBAY_CITY;
					$output["Location - Region"] = "~";
					$output["Location - Country"] = EBAY_COUNTRY;
					$output["Title Bar Image"] = 0;
					if ($image != '') {
						$output["Gallery1.Gallery"] = EBAY_GALLERY;
					} else {
						$output["Gallery1.Gallery"] = "0";
					}
					if (EBAY_GALLERY_FEATURES == 'true' && $image != '') {
						$output["Gallery Featured"] = EBAY_GALLERY_FEATURED;
					} else {
						$output["Gallery Featured"] = "0";
					}
					if (EBAY_GALLERY == 'true' && $image != '') {
						$output["Gallery URL"] = $image;
					} else {
						$output["Gallery URL"] = "~";
					}
					$output["PicInDesc"] = "~";
					$output["PhotoOneRadio"] = "~";
					$output["PhotoOneURL"] = "";
					$output["Gallery2.GalleryPlus"] = "~";	
					$output["Bold"] = EBAY_BOLD;
					$output["MotorsGermanySearchable"] = "~";
					$output["Border"] = EBAY_BORDER;
					$output["LE.Highlight"] = EBAY_HIGHLIGHT;
					$output["Featured Plus"] = EBAY_FEATURED_PLUS;
					$output["Home Page Featured"] = "0";
					$output["Subtitle in search results"] = "~";	
					$output["GiftIcon"] = "0";
					$output["DepositType"] = "0";
					$output["DepositAmount"] = "~";
					$output["ShippingRate"] = "~"; // once eBay support shipping rates, use numinix_shipping_calculator($type, $product_weight=0, $table_zone=0)
					$output["ShippingCarrier"] = "~";
					$output["ShippingType"] = EBAY_SHIPPING_TYPE;
					$output["ShippingPackage"] = "~";
					$output["ShippingIrregular"] = "~";
					$output["ShippingWeightUnit"] = 1;
					$output["WeightMajor"] = zen_major_minor_number(zen_metric_conversion($products->fields['products_weight'], $products->fields['products_weight_type']), 'major');
					$output["WeightMinor"] = zen_major_minor_number(zen_metric_conversion($products->fields['products_weight'], $products->fields['products_weight_type']), 'minor');
					$output["PackageDimension"] = "~";
					$output["ShipFromZipCode"] = '"'. SHIPPING_ORIGIN_ZIP . '"';
					$output["PackagingHandlingCosts"] = "~";
					$output["Year"] = "~";
					$output["MakeCode"] = "~";
					$output["ModelCode"] = "~";
					$output["EngineCode"] = "~";
					$output["ThemeId"] = "~";
					$output["LayoutId"] = "~";
					$output["AutoPay"] = "0";
					$output["Apply Multi-item Shipping Discount"] = EBAY_MULTI;
					$output["Attributes"] = "~";
					if (EBAY_ASA == 'true') {
						$output["Package Length"] = zen_metric_conversion($products->fields['products_length'], $products->fields['products_dim_type']);
						$output["Package Width"] = zen_metric_conversion($products->fields['products_width'], $products->fields['products_dim_type']);
						$output["Package Depth"] = zen_metric_conversion($products->fields['products_height'], $products->fields['products_dim_type']);
					} else {
						$output["Package Length"] = "~";
						$output["Package Width"] = "~";
						$output["Package Depth"] = "~";
					}
					$output["ShippingServiceOptions"] = "";
					$output["VATPercent"] = "~";
					$output["ProductID"] = "";
					$output["UseStockPhotoURLAsGallery"] = "~";
					$output["IncludeStockPhotoURL"] = "~";
					$output["IncludeProductInfo"] = "~";
					$output["UniqueIdentifier"] = "~";
					$output["GiftIcon.GiftWrap"] = "0";
					$output["GiftIcon.GiftExpressShipping"] = "0";
					$output["GiftIcon.GiftShipToRecipient"] = "0";
					$output["InternationalShippingServiceOptions"] = "AAA=";
					$output["Ship-To Locations"] = "AAA=";	
					$output["Zip"] = '"' . SHIPPING_ORIGIN_ZIP . '"';
					$output["NowAndNew"] = "~";
					$output["BuyerRequirements/LinkedPayPalAccount"] = "0";
					$output["PM.PaisaPayAccepted"] = "~";
					$output["LE.ProPackBundle"] = "~";
					$output["BestOfferEnabled"] = EBAY_BEST_OFFER;
					$output["LiveAuctionDetails/LotNumber"] = "~";
					$output["LiveAuctionDetails/SellerSalesNumber"] = "~";
					$output["LiveAuctionDetails/LowEstimate"] = "~";
					$output["LiveAuctionDetails/HighEstimate"] = "~";
					$output["LiveAuctionDetails/eBayBatchNumber"] = "~";
					$output["LiveAuctionDetails/eBayItemInBatch"] = "~";
					$output["LiveAuctionDetails/ScheduleID"] = "~";
					$output["LiveAuctionDetails/UserCatalogID"] = "~";
					$output["Item.ExportedImages"] = "|";
					$output["PhotoDisplayType"] = "~";
					$output["TaxTable"] = EBAY_TAX_TABLE;
					$output["LoanCheck"] = "~";
					$output["CashInPerson"] = "~";
					$output["HoursToDeposit"] = "~";
					$output["DaysToFullPayment"] = "~";
					$output["UserHostedOptimizePictureWellBitmap"] = "~";
					$output["BuyerResponsibleForShipping"] = "0";
					$output["GetItFast"] = "~";
					$output["DispatchTimeMax"] = "~";
					$output["DigitalDeliveryDetails/Requirements"] = "~";
					$output["DigitalDeliveryDetails/Method"] = "~";
					$output["DigitalDeliveryDetails/Instructions"] = "~";
					$output["DigitalDeliveryDetails/URL"] = "~";
					$output["CharityID"] = "~";
					$output["CharityName"] = "~";
					$output["DonationPercentage"] = "~";
					$output["AutoDecline"] = "~";
					$output["ListingDetails/MinimumBestOfferPrice"] = "~";
					$output["ListingDetails/MinimumBestOfferMessage"] = "~";
					$output["LE.ValuePackBundle"] = "0";
					$output["LE.ProPackPlusBundle"] = "0";
					$output["LE.BasicUpgradePackBundle"] = "0";
					$output["LocalOnlyChk"] = "~";
					$output["ListingDetails/LocalListingDistance"] = "~";
					$output["SkypeChat"] = "";
					$output["SkypeVoice"] = "";
					$output["SkypeName"] = "~";
					$output["ContactPrimaryPhone"] = "~";																												
					$output["ContactSecondaryPhone"] = "~";
					$output["ExtendedSellerContactDetails/ClassifiedAdContactByEmailEnabled"] = "~";
					$output["ppl_PhoneEnabled"] = "~";
					$output["BuyerRequirements/ShipToRegistrationCountry"] = "~";
					$output["BuyerRequirements/ZeroFeedbackScore"] = "~";
					$output["BuyerRequirements/MinimumFeedbackScore"] = "~";
					$output["BuyerRequirements/MaximumUnpaidItemStrikes"] = "~";
					$output["BuyerRequirements/MaximumItemRequirements/MaximumItemCount"] = "~";
					$output["BuyerRequirements/MaximumItemRequirements/MinimumFeedbackScore"] = "";
					$output["BuyerRequirements/VerifiedUserRequirements/VerifiedUser"] = "~";
					$output["BuyerRequirements/VerifiedUserRequirements/MinimumFeedbackScore"] = "";
					$output["DisableBuyerRequirements "] = "~";
					$output["Domestic Insurance Option"] = "~";
					$output["Domestic Insurance Amount"] = "~";
					$output["InternationalShippingType"] = EBAY_INT_SHIPPING_TYPE;
					$output["InternationalPackagingHandlingCosts"] = "~";
					$output["ProStores Name"] = "";
					$output["ProStores Enabled"] = "0";
					$output["Domestic Profile Discount"] = "0||";
					$output["International Profile Discount"] = "0||";
					$output["Apply Profile Domestic"] = "0";
					$output["Apply Profile International"] = "0";
									
					zen_ebay_fwrite($output);
				}
			}
			$products->MoveNext();
		}
	
	zen_ebay_fwrite();

	$timer_feed = microtime_float()-$stimer_feed;
	
	echo TEXT_EBAY_FEED_COMPLETE . ' ' . EBAY_TIME_TAKEN . ' ' . sprintf("%f " . TEXT_EBAY_FEED_SECONDS, number_format($timer_feed, 6) ) . ' ' . $anti_timeout_counter . TEXT_EBAY_FEED_RECORDS . NL;	
}
// only need if eBay accepts FTP uploads
/*if (isset($_GET['upload']) && $_GET['upload'] == "yes") {
	echo TEXT_EBAY_UPLOAD_STARTED . NL;
	if(ftp_file_upload(EBAY_SERVER, EBAY_USERNAME, EBAY_PASSWORD, DIR_FS_CATALOG . EBAY_DIRECTORY . EBAY_OUTPUT_FILENAME)) {
		echo TEXT_EBAY_UPLOAD_OK . NL;
		$db->execute("update " . TABLE_CONFIGURATION . " set configuration_value = '" . date("Y/m/d H:i:s") . "' where configuration_key='EBAY_UPLOADED_DATE'");
	} else {
		echo TEXT_EBAY_UPLOAD_FAILED . NL;
	}
}*/

	// IF EBAY EVER OFFERS SHIPPING, THIS WILL BE THE FUNCTION...
	function numinix_shipping_calculator($type, $product_weight=0, $table_zone=0) {
		global $currencies;
		
		switch($type) {
			case "zones_table_rate":
				$table_cost = split("[:,]" , constant('MODULE_SHIPPING_ZONETABLE_COST_' . $table_zone));
				//$table_cost = split("[:,]" , MODULE_SHIPPING_TABLERATE_COST_1);
				$size = sizeof($table_cost);
				for ($i=0, $n=$size; $i<$n; $i+=2) {
					if (round($product_weight,9) <= $table_cost[$i]) {
						$shipping = $table_cost[$i+1];
						break;
					}
				}
				$shipping = $currencies->value($shipping, true, zen_get_currency(EBAY_CURRENCY), $currencies->get_value(zen_get_currency(EBAY_CURRENCY)));
				break;
			default:
				$shipping = '';
				break;
		}
		return $shipping;
	}
	
	function zen_metric_conversion($value, $type) {
		$type = strtolower($type);
		if ($type == "kgs") {
			$LBS = $value * 2.20462262;
			return $LBS;
		} else if ($type == "cm") {
			$IN = $value * 0.393700787;
			return $IN;
		} else if ($type == "lbs") {
			return $value;
		} else if ($type == "in") {
			return $value;
		}
	}
	
	function zen_major_minor_number($number, $type) {
		$major = floor($number);
		if ($major > 0) {
			$minor = round(($number - $major) * 16);
		} else {
			$minor = round($number * 16);
		}
		if (round($minor) == 16) {
			$major++;
			$minor = 0;
		}
		if ($type == 'major') {
			return $major;
		} else if ($type == 'minor') {	
			return $minor;
		}
	}
	

	
	function product_category($categories, $type) {
		$category = explode(",", $categories);
		if ($type == 1) {
			return $category[0];
		} else if ($type == 2) {
			return $category[1];
		}
	}
		
		
	function zen_get_currency($currency) {
		if ($currency == 1) return 'USD';
		if ($currency == 2) return 'CAD';
		if ($currency == 3) return 'GBP';
		if ($currency == 5) return 'AUD';
		if ($currency == 7) return 'EUR';
		if ($currency == 31) return 'CHF';
		if ($currency == 41) return 'TWD';
	}
		
	function zen_price_check($store_price) {
		if (EBAY_MAX_PRICE != 0) { // if max price enabled
			if ($store_price <= EBAY_MAX_PRICE && $store_price >= EBAY_MIN_PRICE) { // if price is within limits
				return true;
			}
		} else if ($store_price >= EBAY_MIN_PRICE) { // if max price is not set and price is within minimum limits (default 0)
			return true;
		} else {
			return false;
		}
	}
		
	function zen_ebay_fwrite($output='') {
		static $fp = false;
		static $output_buffer = "";
		static $title_row = false;
		if($output == '') {
			if(!$fp) {
				$retval = $fp = fopen(DIR_FS_CATALOG . EBAY_DIRECTORY . EBAY_OUTPUT_FILENAME, "wb");
			} else {
				if(strlen($output_buffer) > 0) {
					$retval = fwrite($fp, $output_buffer, strlen($output_buffer));
					$output_buffer = "";
				}
				fclose($fp);
			}
		} else {
			if(!$title_row) {
				$title_row = $output;
			}
			$buf = array();
			foreach($title_row as $key=>$val) {
				$buf[] = (isset($output[$key]) ? $output[$key] : '');
			}
			$output = implode(",", $buf);
			if(strlen($output_buffer) > EBAY_OUTPUT_BUFFER_MAXSIZE) {
				$retval = fwrite($fp, $output_buffer, strlen($output_buffer));
				$output_buffer = "";
			}
			$output = rtrim($output) . "\r\n";
			/*if(strtolower(CHARSET) != 'utf-8')
				$output_buffer .= utf8_encode($output);
			else
				$output_buffer .= $output;*/
			$output_buffer .= $output;
		}
		return $retval;
	}
	
	function ShortenText($text, $chars) {        // Change to the number of characters you want to display                
		$text = $text." ";        
		$text = substr($text,0,$chars);        
		$text = substr($text,0,strrpos($text,' '));      
		return $text;    
	}
	
	function trim_array($x) {
   		if (is_array($x)) {
       		return array_map('trim_array', $x);
   		} else {
   			return trim($x);
		}
	}
	
	function zen_ebay_keywords($words, $string, $charge) {
		if ($words != '') {
			$match = 0;
			$words = split(",", $words);
			foreach ($words as $word) {
				if (strpos(strtolower($string), trim(strtolower($word))) !== false) {
					$match++;
					break;
				}
			}
			if ($match > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			if ($charge == 1) {
				return true;
			} else if ($charge == 2) {
				return false;
			}
		}
	}
					
					 
  function numinix_categories_check($categories_list, $products_id, $charge) {
    $categories_array = split(',', $categories_list);
    if ($categories_list == '') {
      if ($charge == 1) {
        return true;
      } elseif ($charge == 2) {
        return false;
      }
    } else {
      $match = false;
      foreach($categories_array as $category_id) {
        if (zen_product_in_category($products_id, $category_id)) {
          $match = true;
          break;
        }
      }
      if ($match == true) {
        return true;
      } else {
        return false;
      }
    }
  }
	
	function zen_ebay_get_category($products_id) {
		global $categories_array, $db;
		static $p2c;
		if(!$p2c) {
			$q = $db->Execute("SELECT *
												FROM " . TABLE_PRODUCTS_TO_CATEGORIES);
			while (!$q->EOF) {
				if(!isset($p2c[$q->fields['products_id']]))
					$p2c[$q->fields['products_id']] = $q->fields['categories_id'];
				$q->MoveNext();
			}
		}
		if(isset($p2c[$products_id])) {
			$retval = $categories_array[$p2c[$products_id]]['name'];
			$cPath = $categories_array[$p2c[$products_id]]['cPath'];
		} else {
			$cPath = $retval =  "";
		}
		return array($retval, $cPath);
	}

	function zen_ebay_category_tree($id_parent=0, $cPath='', $cName='', $cats=array()){
		global $db, $languages;
		$cat = $db->Execute("SELECT c.categories_id, c.parent_id, cd.categories_name
												 FROM " . TABLE_CATEGORIES . " c
													 LEFT JOIN " . TABLE_CATEGORIES_DESCRIPTION . " cd on c.categories_id = cd.categories_id
												 WHERE c.parent_id = '" . (int)$id_parent . "'
												 AND cd.language_id='" . (int)$languages->fields['languages_id'] . "'
												 AND c.categories_status= '1'",
												 '', false, 150);
		while (!$cat->EOF) {
			$cats[$cat->fields['categories_id']]['name'] = (zen_not_null($cName) ? $cName . ', ' : '') . trim($cat->fields['categories_name']); // previously used zen_ebay_sanita instead of trim
			$cats[$cat->fields['categories_id']]['cPath'] = (zen_not_null($cPath) ? $cPath . '_' : '') . $cat->fields['categories_id'];
			if (zen_has_category_subcategories($cat->fields['categories_id'])) {
				$cats = zen_ebay_category_tree($cat->fields['categories_id'], $cats[$cat->fields['categories_id']]['cPath'], $cats[$cat->fields['categories_id']]['name'], $cats);
			}
			$cat->MoveNext();
		}
		return $cats;
	}

	function zen_ebay_sanita($str, $rt=false) { // currently using zen_ebay_cleaner below instead of zen_ebay_sanita
		$str = strip_tags($str);
		$str = str_replace(array("\t" , "\n", "\r"), ' ', $str);
		$str = preg_replace('/\s\s+/', ' ', $str);
//	$str = str_replace(array("&reg;", "�", "&copy;", "�", "&trade;", "�"), ' ', $str);
		$str = htmlentities(html_entity_decode($str));
		$in = $out = array();
		$in[] = "&reg;"; $out[] = '(r)';
		$in[] = "&copy;"; $out[] = '(c)';
		$in[] = "&trade;"; $out[] = '(tm)';
//		$str = str_replace($in, $out, $str);
		if($rt) {
			$str = str_replace(" ", "&nbsp;", $str);
			$str = str_replace("&nbsp;", "", $str);
		}
		$str = trim($str);
		return $str;
	}
		
	function zen_ebay_cleaner ($str) {
		//$str = str_replace(',', '","', $str);
		$str = str_replace('"', '""', $str);
		$str = trim($str);
		return $str;
	}

	function zen_ebay_image_url($products_image) {
		if($products_image == "") return "";

		$products_image_extention = substr($products_image, strrpos($products_image, '.'));
		$products_image_base = ereg_replace($products_image_extention, '', $products_image);
		$products_image_medium = $products_image_base . IMAGE_SUFFIX_MEDIUM . $products_image_extention;
		$products_image_large = $products_image_base . IMAGE_SUFFIX_LARGE . $products_image_extention;

		// check for a medium image else use small
		if (!file_exists(DIR_WS_IMAGES . 'medium/' . $products_image_medium)) {
		  $products_image_medium = DIR_WS_IMAGES . $products_image;
		} else {
		  $products_image_medium = DIR_WS_IMAGES . 'medium/' . $products_image_medium;
		}
		// check for a large image else use medium else use small
		if (!file_exists(DIR_WS_IMAGES . 'large/' . $products_image_large)) {
		  if (!file_exists(DIR_WS_IMAGES . 'medium/' . $products_image_medium)) {
		    $products_image_large = DIR_WS_IMAGES . $products_image;
		  } else {
		    $products_image_large = DIR_WS_IMAGES . 'medium/' . $products_image_medium;
		  }
		} else {
		  $products_image_large = DIR_WS_IMAGES . 'large/' . $products_image_large;
		}
		if (function_exists('handle_image')) {
			$image_ih = handle_image($products_image_large, '', LARGE_IMAGE_WIDTH, LARGE_IMAGE_HEIGHT, ''); // Change to medium if too much bandwidth from customers.
			$retval = (HTTP_SERVER . DIR_WS_CATALOG . $image_ih[0]);
		} else {
			$retval = (HTTP_SERVER . DIR_WS_CATALOG . $products_image_large);
		}
		return $retval;
	}

	function zen_ebay_expiration_date($base_date) {
		if(EBAY_EXPIRATION_BASE == 'now')
			$expiration_date = time();
		else
			$expiration_date = strtotime($base_date);
		$expiration_date += EBAY_EXPIRATION_DAYS*24*60*60;
		$retval = (date('Y-m-d', $expiration_date));
		return $retval;
	}
// FTP currently not supported
	/*function ftp_file_upload($url, $login, $password, $local_file, $ftp_dir='', $ftp_file=false, $ssl=false, $ftp_mode=FTP_ASCII) {
		if(!is_callable('ftp_connect')) {
			echo FTP_FAILED . NL;
			return false;
		}
		if(!$ftp_file)
			$ftp_file = basename($local_file);
		ob_start();
		if($ssl)
			$cd = ftp_ssl_connect($url);
		else
			$cd = ftp_connect($url);
		if (!$cd) {
			$out = ftp_get_error_from_ob();
			echo FTP_CONNECTION_FAILED . ' ' . $url . NL;
			echo $out . NL;
			return false;
		}
		echo FTP_CONNECTION_OK . ' ' . $url . NL;
		$login_result = ftp_login($cd, $login, $password);
		if (!$login_result) {
			$out = ftp_get_error_from_ob();
//			echo FTP_LOGIN_FAILED . FTP_USERNAME . ' ' . $login . FTP_PASSWORD . ' ' . $password . NL;
			echo FTP_LOGIN_FAILED . NL;
			echo $out . NL;
			ftp_close($cd);
			return false;
		}
//		echo FTP_LOGIN_OK . FTP_USERNAME . ' ' . $login . FTP_PASSWORD . ' ' . $password . NL;
		echo FTP_LOGIN_OK . NL;
		if ($ftp_dir != "") {
			if (!ftp_chdir($cd, $ftp_dir)) {
				$out = ftp_get_error_from_ob();
				echo FTP_CANT_CHANGE_DIRECTORY . '&nbsp;' . $url . NL;
				echo $out . NL;
				ftp_close($cd);
				return false;
			}
		}
		echo FTP_CURRENT_DIRECTORY . '&nbsp;' . ftp_pwd($cd) . NL;
		ftp_pasv($cd, true);
		$upload = ftp_put($cd, $ftp_file, $local_file, $ftp_mode);
		$out = ftp_get_error_from_ob();
		$raw = ftp_rawlist($cd, $ftp_file, true);
		for($i=0,$n=sizeof($raw);$i<$n;$i++){
			$out .= $raw[$i] . '<br/>';
		}
		if (!$upload) {
			echo FTP_UPLOAD_FAILED . NL;
			if(isset($raw[0])) echo $raw[0] . NL;
			echo $out . NL;
			ftp_close($cd);
			return false;
		} else {
			echo FTP_UPLOAD_SUCCESS . NL;
			echo $raw[0] . NL;
			echo $out . NL;
		}
		ftp_close($cd);
		return true;
	}*/
// FTP currently not supported
	/*function ftp_get_error_from_ob() {
		$out = ob_get_contents();
		ob_end_clean();
		$out = str_replace(array('\\', '<!--error-->', '<br>', '<br />', "\n", 'in <b>'),array('/', '', '', '', '', ''),$out);
		if(strpos($out, DIR_FS_CATALOG) !== false){
			$out = substr($out, 0, strpos($out, DIR_FS_CATALOG));
		}
		return $out;
	}*/

function microtime_float() {
   list($usec, $sec) = explode(" ", microtime());
   return ((float)$usec + (float)$sec);
}
?>