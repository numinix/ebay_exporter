<?php
/**
 * ebay.php
 *
 * @package eBay Turbo Lister 2.0 Exporter
 * @copyright Copyright 2007 Numinix Technology http://www.numinix.com
 * @copyright Portions Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: ebay.php, v1.0.0 30.07.2007 22:05 numinix $
 */
 
define('TEXT_EBAY_STARTED', 'eBay Turbo Lister 2.0 Exporter v.' . EBAY_VERSION . ' started ' . date("Y/m/d H:i:s"));
define('TEXT_EBAY_FILE_LOCATION', 'Feed file - ');
define('TEXT_EBAY_FEED_COMPLETE', 'eBay File Complete');
define('TEXT_EBAY_FEED_TIMER', 'Time:');
define('TEXT_EBAY_FEED_SECONDS', 'Seconds');
define('TEXT_EBAY_FEED_RECORDS', ' Records');
define('EBAY_TIME_TAKEN', 'In');
define('EBAY_VIEW_FILE', 'View File:');
define('ERROR_EBAY_DIRECTORY_NOT_WRITEABLE', 'Your eBay folder is not writeable! Please chmod the /' . EBAY_DIRECTORY . ' folder to 755 or 777 depending on your host.');
define('ERROR_EBAY_DIRECTORY_DOES_NOT_EXIST', 'Your eBay output directory does not exist! Please create an /' . EBAY_DIRECTORY . ' directory and chmod to 755 or 777 depending on your host.');
define('ERROR_EBAY_OPEN_FILE', 'Error opening eBay output file "' . DIR_FS_CATALOG . EBAY_DIRECTORY . EBAY_OUTPUT_FILENAME . '"');
define('TEXT_EBAY_ERRSETUP', 'eBay error setup:');
define('TEXT_EBAY_ERRSETUP_L', 'eBay Feed Language "%s" not defined in zen-cart store.');
define('TEXT_EBAY_ERRSETUP_C', 'eBay Default Currency "%s" not defined in zen-cart store.');
?>