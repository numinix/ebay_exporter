# eBay Bulk File Generator

SET @configuration_group_id=0;
SELECT @configuration_group_id:=configuration_group_id
FROM configuration_group
WHERE configuration_group_title= 'eBay Exporter Configuration'
LIMIT 1;
DELETE FROM configuration WHERE configuration_group_id = @configuration_group_id AND @configuration_group_id != 0;
DELETE FROM configuration_group WHERE configuration_group_id = @configuration_group_id AND @configuration_group_id != 0;

INSERT INTO configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) VALUES (NULL, 'eBay Exporter Configuration', 'Set eBay Options', '1', '1');
SET @configuration_group_id=last_insert_id();
UPDATE configuration_group SET sort_order = @configuration_group_id WHERE configuration_group_id = @configuration_group_id;

INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES 
(NULL, 'Output File Name', 'EBAY_OUTPUT_FILENAME', 'ebay_inventory.csv', 'Set the name of your eBay output file', @configuration_group_id, 0, NOW(), NULL, NULL),
(NULL, 'Output Directory', 'EBAY_DIRECTORY', 'feed/', 'Set the name of your eBay output directory', @configuration_group_id, 1, NOW(), NULL, NULL),
(NULL, 'Use cPath in url', 'EBAY_USE_CPATH', 'false', 'Use cPath in product info url', @configuration_group_id, 2, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Compress Feed File', 'EBAY_COMPRESS', 'false', 'Compress eBay file?', @configuration_group_id, 3, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Zen Cart/Ebay Attributes', 'EBAY_ASA', 'false', 'Zen Cart/Ebay Attributes ?(N/B: Requires Numinix Product Fields with eBay optional fields) ', @configuration_group_id, 3, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'eBay Categories ID', 'EBAY_MAIN_CATEGORIES', '171993', 'You have to enter the correct/precise eBay Categories ID pertaining to your store. Please change the default ID "171993" to match the one for your site.', @configuration_group_id, 4, NOW(), NULL, NULL),
(NULL, 'Included Categories', 'EBAY_POS_CATEGORIES', '', 'Enter category names separated by commas <br>(i.e. 1,2,3)<br />Leave blank to allow all categories', @configuration_group_id, 4, NOW(), NULL, NULL),
(NULL, 'Excluded Categories', 'EBAY_NEG_CATEGORIES', '', 'Enter category names separated by commas <br>(i.e. 1,2,3)<br />Leave blank to deactivate', @configuration_group_id, 4, NOW(), NULL, NULL),
(NULL, 'Excluded Keywords', 'EBAY_NEG_KEYWORDS', 'oem', 'Enter title keywords separated by commas <br> (i.e. oem,gift,free,)<br />Leave blank to deactivate', @configuration_group_id, 4, NOW(), NULL, NULL),
(NULL, 'Max products', 'EBAY_MAX_PRODUCTS', '0', 'Default = 0 for infinite # of products', @configuration_group_id, 5, NOW(), NULL, NULL),
(NULL, 'Starting Point', 'EBAY_START_PRODUCTS', '0', 'Start at which entry (not product_id)?<br />Default=0', @configuration_group_id, 5, NOW(), NULL, NULL),
(NULL, 'Min Price', 'EBAY_MIN_PRICE', '0', 'Default = 0 for no minimum', @configuration_group_id, 6, NOW(), NULL, NULL),
(NULL, 'Max Price', 'EBAY_MAX_PRICE', '0', 'Default = 0 for no maximum', @configuration_group_id, 6, NOW(), NULL, NULL),

(NULL, 'SiteID', 'EBAY_SITEID', '0', 'For what eBay site?<br />eBay US = 0<br />eBay Canada = 2<br />eBay UK = 3<br />eBay Australia = 15<br />eBay Belgium (French) = 23<br />eBay France = 71<br />eBay Germany = 77<br />eBay Italy = 101<br />eBay Belgium (Dutch) = 123<br />eBay Netherlands = 146<br />eBay Spain = 186<br />eBay Taiwan = 196)', @configuration_group_id, 7, NOW(), NULL, 'zen_cfg_select_option(array(\'0\', \'2\', \'3\', \'15\', \'23\', \'71\', \'77\', \'101\', \'123\', \'146\', \'186\', \'196\'),'),
(NULL, 'Format', 'EBAY_FORMAT', '1', 'Listing format<br />Auction = 1<br />Dutch Auction = 2<br />Live Auctions = 5<br />Ad Format = 6<br />Store Inventory = 7<br />Fixed Price = 9', @configuration_group_id, 8, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'2\', \'5\', \'6\', \'7\', \'9\'),'),
(NULL, 'Currency', 'EBAY_CURRENCY', '1', 'Select currency:<br />USD = 1<br />CAD = 2<br />GBP = 3<br />AUD = 5<br />EUR = 7<br />CHF = 13<br />TWD = 41', @configuration_group_id, 9, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'2\', \'3\', \'5\', \'7\', \'13\', \'41\'),'),
(NULL, 'Fixed Starting Price', 'EBAY_FIXED_STARTING', '', 'What price do you want to start your auction?<br />Leave blank to use variable starting price.', @configuration_group_id, 10, NOW(), NULL, NULL),
(NULL, 'Variable Starting Price', 'EBAY_STARTING', '100', 'What percentage of the store price do you want your starting price to be?<br />Default = 100', @configuration_group_id, 11, NOW(), NULL, NULL),
(NULL, 'Reserve Price', 'EBAY_RESERVE', '0', 'What percentage of the store price do you want your reserve price to be?<br />Default = 0', @configuration_group_id, 12, NOW(), NULL, NULL),
(NULL, 'Buy It Now', 'EBAY_BIN', '0', 'What percentage of the store price do you want your Buy It Now price to be?<br />Default = 0', @configuration_group_id, 13, NOW(), NULL, NULL),
(NULL, 'Duration', 'EBAY_DURATION', '7', 'Duration in days<br />default = 7', @configuration_group_id, 14, NOW(), NULL, 'zen_cfg_select_option(array(\'3\', \'5\', \'7\', \'10\', \'20\', \'30\', \'60\', \'90\', \'120\'),'),
(NULL, 'Quantity', 'EBAY_QUANTITY', '', 'The number of items you are selling on this auction.<br />Leave blank to include all stock.', @configuration_group_id, 15, NOW(), NULL, NULL),
(NULL, 'Insurance', 'EBAY_INSURANCE', '0', 'Insurance options<br />None = 0<br />Optional = 1<br />Required = 2<br />Included in S&H = 3', @configuration_group_id, 16, NOW(), NULL, 'zen_cfg_select_option(array(\'0\', \'1\', \'2\', \'3\'),'),
(NULL, 'Country', 'EBAY_COUNTRY', 'US', 'Enter two digit country code (i.e. US, CA, UK)', @configuration_group_id, 17, NOW(), NULL, NULL),
(NULL, 'City/State', 'EBAY_CITY', '', 'Name of city or state<br />(i.e., Vancouver, British Columbia, Los Angeles)', @configuration_group_id, 18, NOW(), NULL, NULL),

(NULL, 'Tax Table', 'EBAY_TAX_TABLE', '0', 'Use Tax Table?', @configuration_group_id, 19, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'Apply Tax', 'EBAY_TAX', '0', 'Apply Tax To Total?', @configuration_group_id, 19, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'Private Auction', 'EBAY_PRIVATE', '0', 'Private Auction?', @configuration_group_id, 20, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'Multi-Item Discount', 'EBAY_MULTI', '0', 'Apply multi-item shipping discount?', @configuration_group_id, 20, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'Best Offers', 'EBAY_BEST_OFFER', '0', 'Enable Best Offer?', @configuration_group_id, 20, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'Custom Label', 'EBAY_LABEL', 'id', 'What do you want to use for the custom label?', @configuration_group_id, 20, NOW(), NULL, 'zen_cfg_select_option(array(\'id\', \'model\', \'upc\'),'),
(NULL, 'PayPal', 'EBAY_PAYPAL_ACCEPTED', '0', 'Accept PayPal?', @configuration_group_id, 21, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'PayPal Email', 'EBAY_PAYPAL_ADDRESS', '', 'PayPal email address', @configuration_group_id, 21, NOW(), NULL, NULL),

(NULL, 'Pickup', 'EBAY_PICKUP', '0', 'Offer pickup?', @configuration_group_id, 22, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'CC', 'EBAY_CC', '0', 'Accept Credit Cards?', @configuration_group_id, 22, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'AmEx', 'EBAY_AMEX', '0', 'Accept American Express?', @configuration_group_id, 22, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'Discover', 'EBAY_DISCOVER', '0', 'Accept Discover?', @configuration_group_id, 22, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'Visa Mastercard', 'EBAY_VISA', '0', 'Accept Visa/Mastercard?', @configuration_group_id, 22, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'COD', 'EBAY_COD', '0', 'Accept Cash on Delivery?', @configuration_group_id, 22, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'COD Prepay', 'EBAY_COD_PREPAY', '0', 'Accept COD w/ Prepaid Shipping?', @configuration_group_id, 22, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'Postal Transfer', 'EBAY_POSTAL_TRANSFER', '0', 'Accept Postal Money Transfer?', @configuration_group_id, 22, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'Money Order', 'EBAY_MONEYORDER', '0', 'Accept Money Order?', @configuration_group_id, 22, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'Personal Check', 'EBAY_CHECK', '0', 'Accept Personal Checks?', @configuration_group_id, 22, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'Money Transfer', 'EBAY_MX', '0', 'Accept Money Transfer?', @configuration_group_id, 22, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'Money Transfer in Checkout', 'EBAY_MX_CHECKOUT', '0', 'Accept Money Transfer in Checkout?', @configuration_group_id, 22, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'Other Payment', 'EBAY_OTHER_PAYMENT', '0', 'Accept Other Payments?', @configuration_group_id, 22, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'Other Online Payment', 'EBAY_OTHER_ONLINE', '0', 'Accept Other Online Payments?', @configuration_group_id, 22, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'See Description For Shipping', 'EBAY_SEE_DESCRIPTION', '0', 'See description for shipping options?', @configuration_group_id, 22, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),

(NULL, 'Shipping Type', 'EBAY_SHIPPING_TYPE', '1', '1 = Flat shipping rates, 2 = Calculated shipping rates', @configuration_group_id, 23, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'2\'),'),
(NULL, 'International Shipping Type', 'EBAY_INT_SHIPPING_TYPE', '1', '1 = Flat shipping rates, 2 = Calculated shipping rates', @configuration_group_id, 23, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'2\'),'),
(NULL, 'Pay Shipping', 'EBAY_PAYS_SHIPPING', 'buyer', 'Who pays the shipping?', @configuration_group_id, 23, NOW(), NULL, 'zen_cfg_select_option(array(\'buyer\', \'seller\'),'),

(NULL, 'Subtitle', 'EBAY_SUBTITLE', 'false', 'Use ASA Phrase 1 as Subtitle (max length = 55 chars)?', @configuration_group_id, 24, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Counter', 'EBAY_COUNTER', '2', '0 = No Counter<br />5 = Basic<br />6 = Retro-Computer Style <br />7 = Hidden', @configuration_group_id, 99, NOW(), NULL, 'zen_cfg_select_option(array(\'0\', \'5\', \'6\', \'7\'),'),
(NULL, 'Bold', 'EBAY_BOLD', '0', 'Bold Title?', @configuration_group_id, 24, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Featured', 'EBAY_FEATURED', '0', 'Featured Listing?', @configuration_group_id, 25, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Featured Plus', 'EBAY_FEATURED_PLUS', '0', 'Featured Plus Listing?', @configuration_group_id, 25, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Gallery', 'EBAY_GALLERY', '0', 'Gallery?', @configuration_group_id, 26, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'Gallery Featured', 'EBAY_GALLERY_FEATURED', '0', 'Gallery?', @configuration_group_id, 26, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'Highlight', 'EBAY_HIGHLIGHT', '0', 'Highlight Title?', @configuration_group_id, 24, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),
(NULL, 'Border', 'EBAY_BORDER', '0', 'Put Border Around Title?', @configuration_group_id, 24, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'0\'),'),

(NULL, 'eBay Template Top', 'EBAY_TEMPLATE_TOP', '', "Enter the HTML to go before each product's description.", @configuration_group_id, 30, NOW(), NULL, NULL),
(NULL, 'eBay Template Bottom', 'EBAY_TEMPLATE_BOTTOM', '', "Enter the HTML to go after each product's description.", @configuration_group_id, 31, NOW(), NULL, NULL);

# Register the configuration page for Admin Access Control
INSERT IGNORE INTO admin_pages (page_key,language_key,main_page,page_params,menu_key,display_on_menu,sort_order) VALUES ('configeBayexporter','BOX_CONFIGURATION_EBAYEXPORTER','FILENAME_CONFIGURATION',CONCAT('gID=',@configuration_group_id),'configuration','Y',@configuration_group_id);

# Register the tools page for Admin Access Control
INSERT IGNORE INTO admin_pages (`page_key`, `language_key`, `main_page`, `page_params`, `menu_key`, `display_on_menu`, `sort_order`) VALUES ('toolseBayExporter', 'BOX_EBAY', 'FILENAME_EBAY', '', 'tools', 'Y', @configuration_group_id);  